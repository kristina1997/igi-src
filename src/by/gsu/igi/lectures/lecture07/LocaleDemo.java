package by.gsu.igi.lectures.lecture07;

import java.util.Locale;

/**
 * @author Evgeniy Myslovets
 */
public class LocaleDemo {

    public static void main(String[] args) {
        System.out.println(Locale.getDefault());
        System.out.println(Locale.FRENCH);
        System.out.println(Locale.FRANCE);
        Locale belorussian = new Locale("by", "BY");
        Locale.setDefault(belorussian);
        System.out.println(Locale.getDefault());
    }
}
