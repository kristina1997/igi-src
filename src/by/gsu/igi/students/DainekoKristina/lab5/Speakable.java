package by.gsu.igi.students.DainekoKristina.lab5;

/**
 * Created by DainekoKristina.
 */
public interface Speakable {
    void speak();
}
