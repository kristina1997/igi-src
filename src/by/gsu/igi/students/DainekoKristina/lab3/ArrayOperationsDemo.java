package by.gsu.igi.students.DainekoKristina.lab3;

import java.util.Scanner;

/**
 * Created by Evgeniy Myslovets.
 */
public class ArrayOperationsDemo {

    public static int findMax(int[] numbers) {
        {
            int max = numbers[0];
            for (int i = 1; i < numbers.length; i++) {
                if (max < numbers[i]) {
                    max = numbers[i];

                }
            }
            return max;
        }
    }

    public static int findMin(int[] numbers) {
        int min = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (min > numbers[i])
                min = numbers[i];
        }
        return min;
    }

    public static int calculateAverage(int[] numbers) {
        int everage = 0;
        for (int number : numbers) {

           everage = number + everage;
        }

        return (everage / numbers.length);
    }

    public static int product(int[] numbers) {
        int proc;
        proc = 1;
        for (int i = 0; i < numbers.length; i++) {
            proc = proc * numbers[i];
        }
        return proc;
    }

    public static int sum(int[] numbers) {
        int sum = 0;
        for (int number : numbers) {
            sum = sum + number;
        }
        return sum;
    }

    public static int difference(int[] numbers) {
        int diff=0;
        for(int i = 0; i<numbers.length; i++){
            diff=diff - numbers[i];
        }
        return diff;
    }

    public static void main(String[] args) {
        // читаем из консоли последовательность целых чисел,
        int[] numbers = readArray();

        // вызываем вычислительные методы для полученного массива
        processArray(numbers);
    }

    private static int[] readArray() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество элементов:");
        int size = scanner.nextInt();

        int[] numbers = new int[size];
        for (int i = 0; i < size; i++) {
            System.out.println("Введите " + (i + 1) + "-й элемент:");
            numbers[i] = scanner.nextInt();
        }

        return numbers;
    }

    private static void processArray(int[] numbers) {
        int max = findMax(numbers);
        System.out.println("Максимальный элемент: " + max);

        int min = findMin(numbers);
        System.out.println("Минимальный элемент: " + min);

        int average = calculateAverage(numbers);
        System.out.println("Среднее значение: " + average);

        int product = product(numbers);
        System.out.println("Произведение элементов: " + product);

        int sum = sum(numbers);
        System.out.println("Сумма элементов: " + sum);

        int difference = difference(numbers);
        System.out.println("Разность элементов: " + difference);
    }
}
